import Vue from 'vue'
import {
  Container,
  Aside,
  Input,
  Tree,
  Header,
  Main,
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import vueHeadful from 'vue-headful';
Vue.component('vue-headful', vueHeadful);

locale.use(lang)

Vue.use(Container)
Vue.use(Aside)
Vue.use(Input)
Vue.use(Tree)
Vue.use(Header)
Vue.use(Main)
