var staticTreeData = {
  "title": "Test Images for Demo",
  "timestamp": "2019-01-25 11:01",
  "data":[
    {
      "id": 1,
      "label": "Level one 1",
      "children": [
        {
          "id": 4,
          "label": "Level two 1-1",
          "children": [
            {
              "id": 9,
              "label": "Level three 1-1-1",
              "title": "Image 500-1",
              "image": "500-1.jpeg"
            },
            {
              "id": 10,
              "label": "Level three 1-1-2",
              "title": "Image 500-2",
              "image": "500-2.jpeg",
              "timestamp": "2019-01-22 11:01"
            }
          ]
        }
      ]
    },
    {
      "id": 2,
      "label": "Level one 2",
      "children": [
        {
          "id": 5,
          "label": "Level two 2-1",
          "title": "Image 500-3",
          "image": "500-3.jpeg",
          "timestamp": "2019-01-23 11:01"
        },
        {
          "id": 6,
          "label": "Level two 2-2",
          "title": "Image 500-4",
          "image": "500-4.jpeg",
          "timestamp": "2019-01-24 11:01"
        }
      ]
    },
    {
      "id": 3,
      "label": "Level one 3",
      "children": [
        {
          "id": 7,
          "label": "Level two 3-1",
          "title": "Image 500-5",
          "image": "500-5.jpeg",
          "timestamp": "2019-01-25 11:01"
        },
        {
          "id": 8,
          "label": "Level two 3-2",
          "title": "Image 500-6",
          "image": "500-6.jpeg",
          "timestamp": "2019-01-26 11:01"
        }
      ]
    },
    {
      "id": 1,
      "label": "Level one 1",
      "children": [
        {
          "id": 4,
          "label": "Level two 1-1",
          "children": [
            {
              "id": 9,
              "label": "Level three 1-1-1",
              "title": "Image 500-1",
              "image": "500-1.jpeg"
            },
            {
              "id": 10,
              "label": "Level three 1-1-2",
              "title": "Image 500-2",
              "image": "500-2.jpeg",
              "timestamp": "2019-01-22 11:01"
            }
          ]
        }
      ]
    },
    {
      "id": 2,
      "label": "Level one 2",
      "children": [
        {
          "id": 5,
          "label": "Level two 2-1",
          "title": "Image 500-3",
          "image": "500-3.jpeg",
          "timestamp": "2019-01-23 11:01"
        },
        {
          "id": 6,
          "label": "Level two 2-2",
          "title": "Image 500-4",
          "image": "500-4.jpeg",
          "timestamp": "2019-01-24 11:01"
        }
      ]
    },
    {
      "id": 3,
      "label": "Level one 3",
      "children": [
        {
          "id": 7,
          "label": "Level two 3-1",
          "title": "Image 500-5",
          "image": "500-5.jpeg",
          "timestamp": "2019-01-25 11:01"
        },
        {
          "id": 8,
          "label": "Level two 3-2",
          "title": "Image 500-6",
          "image": "500-6.jpeg",
          "timestamp": "2019-01-26 11:01"
        }
      ]
    },
    {
      "id": 1,
      "label": "Level one 1",
      "children": [
        {
          "id": 4,
          "label": "Level two 1-1",
          "children": [
            {
              "id": 9,
              "label": "Level three 1-1-1",
              "title": "Image 500-1",
              "image": "500-1.jpeg"
            },
            {
              "id": 10,
              "label": "Level three 1-1-2",
              "title": "Image 500-2",
              "image": "500-2.jpeg",
              "timestamp": "2019-01-22 11:01"
            }
          ]
        }
      ]
    },
    {
      "id": 2,
      "label": "Level one 2",
      "children": [
        {
          "id": 5,
          "label": "Level two 2-1",
          "title": "Image 500-3",
          "image": "500-3.jpeg",
          "timestamp": "2019-01-23 11:01"
        },
        {
          "id": 6,
          "label": "Level two 2-2",
          "title": "Image 500-4",
          "image": "500-4.jpeg",
          "timestamp": "2019-01-24 11:01"
        }
      ]
    },
    {
      "id": 3,
      "label": "Level one 3",
      "children": [
        {
          "id": 7,
          "label": "Level two 3-1",
          "title": "Image 500-5",
          "image": "500-5.jpeg",
          "timestamp": "2019-01-25 11:01"
        },
        {
          "id": 8,
          "label": "Level two 3-2",
          "title": "Image 500-6",
          "image": "500-6.jpeg",
          "timestamp": "2019-01-26 11:01"
        }
      ]
    },
    {
      "id": 1,
      "label": "Level one 1",
      "children": [
        {
          "id": 4,
          "label": "Level two 1-1",
          "children": [
            {
              "id": 9,
              "label": "Level three 1-1-1",
              "title": "Image 500-1",
              "image": "500-1.jpeg"
            },
            {
              "id": 10,
              "label": "Level three 1-1-2",
              "title": "Image 500-2",
              "image": "500-2.jpeg",
              "timestamp": "2019-01-22 11:01"
            }
          ]
        }
      ]
    },
    {
      "id": 2,
      "label": "Level one 2",
      "children": [
        {
          "id": 5,
          "label": "Level two 2-1",
          "title": "Image 500-3",
          "image": "500-3.jpeg",
          "timestamp": "2019-01-23 11:01"
        },
        {
          "id": 6,
          "label": "Level two 2-2",
          "title": "Image 500-4",
          "image": "500-4.jpeg",
          "timestamp": "2019-01-24 11:01"
        }
      ]
    },
    {
      "id": 3,
      "label": "Level one 3",
      "children": [
        {
          "id": 7,
          "label": "Level two 3-1",
          "title": "Image 500-5",
          "image": "500-5.jpeg",
          "timestamp": "2019-01-25 11:01"
        },
        {
          "id": 8,
          "label": "Level two 3-2",
          "title": "Image 500-6",
          "image": "500-6.jpeg",
          "timestamp": "2019-01-26 11:01"
        }
      ]
    },
    {
      "id": 1,
      "label": "Level one 1",
      "children": [
        {
          "id": 4,
          "label": "Level two 1-1",
          "children": [
            {
              "id": 9,
              "label": "Level three 1-1-1",
              "title": "Image 500-1",
              "image": "500-1.jpeg"
            },
            {
              "id": 10,
              "label": "Level three 1-1-2",
              "title": "Image 500-2",
              "image": "500-2.jpeg",
              "timestamp": "2019-01-22 11:01"
            }
          ]
        }
      ]
    },
    {
      "id": 2,
      "label": "Level one 2",
      "children": [
        {
          "id": 5,
          "label": "Level two 2-1",
          "title": "Image 500-3",
          "image": "500-3.jpeg",
          "timestamp": "2019-01-23 11:01"
        },
        {
          "id": 6,
          "label": "Level two 2-2",
          "title": "Image 500-4",
          "image": "500-4.jpeg",
          "timestamp": "2019-01-24 11:01"
        }
      ]
    },
    {
      "id": 3,
      "label": "Level one 3",
      "children": [
        {
          "id": 7,
          "label": "Level two 3-1",
          "title": "Image 500-5",
          "image": "500-5.jpeg",
          "timestamp": "2019-01-25 11:01"
        },
        {
          "id": 8,
          "label": "Level two 3-2",
          "title": "Image 500-6",
          "image": "500-6.jpeg",
          "timestamp": "2019-01-26 11:01"
        }
      ]
    }
  ]
};

